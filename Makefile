all:
	xelatex -interaction=nonstopmode tesi
	xelatex -interaction=nonstopmode tesi-frn
	xelatex -interaction=nonstopmode tesi
	bibtex tesi
	xelatex -interaction=nonstopmode tesi
	xelatex -interaction=nonstopmode tesi

pdfa:
	touch pdfa
	xelatex -interaction=nonstopmode -shell-escape -output-driver="xdvipdfmx -z 0" tesi
	xelatex -interaction=nonstopmode -shell-escape -output-driver="xdvipdfmx -z 0" tesi-frn
	xelatex -interaction=nonstopmode -shell-escape -output-driver="xdvipdfmx -z 0" tesi
	bibtex tesi
	xelatex -interaction=nonstopmode -shell-escape -output-driver="xdvipdfmx -z 0" tesi
	xelatex -interaction=nonstopmode -shell-escape -output-driver="xdvipdfmx -z 0" tesi
	rm pdfa

clean:
	rm tesi.pdf

