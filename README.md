## Compilare `tesi.tex`

La compilazione della tesi richiede `texlive` completo.

```bash
$ make # produce tesi.pdf in pdf normale
$ make pdfa # produce tesi.pdf in pdf/a
$ make clean # elimina tesi.pdf
```

## Compilare `presentazione/presentazione.tex`

La compilazione della tesi richiede `texlive` completo e `beamer`.

```bash
$ cd presentazione
$ make # produce presentazione.pdf
$ make clean # elimina presentazione.pdf
```

## Compilare `riassunto/riassunto.md`

La compilazione della tesi richiede `pandoc` e `texlive` completo.

```bash
$ cd riassunto
$ make # produce riassunto.pdf
$ make odt # produce riassunto.odt
$ make clean # elimina riassunto.pdf e riassunto.odt
```